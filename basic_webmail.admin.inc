<?php

/**
 * @file
 * Administrative functions for Basic webmail module.
 */

/**
 * Administration settings form.
 */
function basic_webmail_admin_settings() {
  $form = array();

  // General settings.
  $form['general'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('General settings'),
    '#collapsible' => TRUE,
  );
  $form['general']['basic_webmail_account_info'] = array(
    '#type'          => 'checkbox',
    '#title'         => t("Use the user's account e-mail address and password
      for checking e-mail."),
    '#default_value' => variable_get('basic_webmail_account_info', 1),
    '#description'   => t("Instead of providing additional form fields to get
      the user's e-mail address and password, just use what is provided within
      the account settings."),
  );
  $form['general']['basic_webmail_messages_per_page'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Messages per page'),
    '#size'          => 10,
    '#default_value' => variable_get('basic_webmail_messages_per_page', 25),
    '#description'   => t('The number of messages to show per page when viewing
      the listing.'),
  );
  $form['general']['basic_webmail_subject_characters'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of characters to display'),
    '#size'          => 10,
    '#default_value' => variable_get('basic_webmail_subject_characters', 40),
    '#description'   => t('This is the number of characters of the subject that
      are displayed in the message list before being truncated. Entering a zero
      (0) here will cause the listing to display all characters of the subject
      line, no matter how long it is.'),
  );
  $time = time();
  $form['general']['basic_webmail_format_option'] = array(
    '#type'          => 'radios',
    '#title'         => t('Format option'),
    '#options'       => array(
      'small'  => t('Small (!date)',
        array('!date' => format_date($time, 'small'))),
      'medium' => t('Medium (!date)',
        array('!date' => format_date($time))),
      'large'  => t('Large (!date)',
        array('!date' => format_date($time, 'large'))),
      'custom' => t('Custom'),
    ),
    '#default_value' => variable_get('basic_webmail_format_option', 'small'),
    '#description'   => t('Specify how you want the date of the message
      displayed.'),
  );
  $form['general']['basic_webmail_custom_format'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Custom format'),
    '#default_value' => variable_get('basic_webmail_custom_format',
      'D, M j, Y - g:i:s a'),
    '#description'   => t('Specify how you want the Custom date format
      configured, using the format options of the PHP
      <a href="http://php.net/manual/en/function.date.php">date()</a>
      function.'),
  );
  $form['general']['basic_webmail_number_attachments'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of attachments'),
    '#size'          => 10,
    '#default_value' => variable_get('basic_webmail_number_attachments', 3),
    '#description'   => t("The number of attachments to allow on the contact
      form. The maximum number of allowed uploads may be limited by PHP. If
      necessary, check your system's PHP php.ini file for a max_file_uploads
      directive to change."),
  );
  $form['general']['basic_webmail_attachment_location'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Attachment location'),
    '#default_value' => variable_get('basic_webmail_attachment_location',
      file_directory_path() . '/attachments'),
    '#description'   => t('The file path where to save message attachments.'),
  );

  // Server settings.
  $form['server'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Server settings'),
    '#collapsible' => TRUE,
  );
  $form['server']['basic_webmail_server_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Server address'),
    '#default_value' => variable_get('basic_webmail_server_address', ''),
    '#description'   => t('The address to the server you wish to connect to.'),
  );
  $form['server']['basic_webmail_server_port'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Connection port'),
    '#size'          => 10,
    '#default_value' => variable_get('basic_webmail_server_port', 143),
    '#description'   => t('The default IMAP port is 143 if one is not
      specified.'),
  );
  $form['server']['basic_webmail_secure_log_in'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Secure login.'),
    '#default_value' => variable_get('basic_webmail_secure_log_in', 0),
    '#description'   => t('Check to make a secure connection to your IMAP
      Server.'),
  );
  $form['server']['basic_webmail_use_ssl'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Encrypt session using SSL.'),
    '#default_value' => variable_get('basic_webmail_use_ssl', 0),
    '#description'   => t('Use SSL to connect to the server.'),
  );
  $form['server']['basic_webmail_validate_cert'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Validate certificate.'),
    '#default_value' => variable_get('basic_webmail_validate_cert', 0),
    '#description'   => t('When using a secure connection, validate the
      certificate.'),
  );
  $form['server']['basic_webmail_use_tls'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Encrypt session using TLS.'),
    '#default_value' => variable_get('basic_webmail_use_tls', 0),
    '#description'   => t('Use TLS to connect to the server.'),
  );

  // User permissions.
  $form['permissions'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('User permissions'),
    '#collapsible' => TRUE,
  );
  $form['permissions']['basic_webmail_user_allow_configure'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Allow users to configure their mail accounts.'),
    '#default_value' => variable_get('basic_webmail_user_allow_configure', 0),
    '#description'   => t('Check to allow users configure their mail
      login/password on the profile settings page.'),
  );
  //delete copy move forward reply compose
  $form['permissions']['basic_webmail_user_allowed_actions'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select actions which user can perform'),
    '#options'       => array(
      'delete'  => t('Delete'),
      'move'    => t('Move'),
      'copy'    => t('Copy'),
      'forward' => t('Forward'),
      'reply'   => t('Reply'),
      'sendmail' => t('Send mails'),
    ),
    '#default_value' => basic_webmail_get_user_actions(),
    '#description'   => t('Note, if you allow to Forward withow allowing to
      Send mails, user will not be able to edit text of forwarded message.'),
  );

  return system_settings_form($form);
}

/**
 * Vaidation handler for the administration settings form.
 */
function basic_webmail_admin_settings_validate($form, &$form_state) {

  $format_option = trim($form_state['values']['basic_webmail_format_option']);
  $custom_format = trim($form_state['values']['basic_webmail_custom_format']);
  if ($format_option == 'custom' && $custom_format == '') {
    form_set_error('basic_webmail_custom_format', t('You have specified Custom
      as your Format option for your Date format, but you have not specified the
      configuration of the Cuatom format. Either choose a different Format
      option, or specify the configuration of the Custom format.'));
  }

  $attach_location = $form_state['values']['basic_webmail_attachment_location'];
  if (!file_check_directory($attach_location, FILE_CREATE_DIRECTORY)) {
    form_set_error('basic_webmail_attachment_location', t('The directory does
      not exist or is not writable, and there was a problem creating the
      path.'));
  }

  $use_ssl = $form_state['values']['basic_webmail_use_ssl'];
  $server_port = $form_state['values']['basic_webmail_server_port'];
  if ($use_ssl && $server_port == '143') {
    form_set_error('basic_webmail_server_port',
      t('The normal port for secure IMAP is 993.'));
  }
}
