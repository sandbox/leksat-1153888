<?php

/**
 * @file
 * Common functions for Basic webmail module.
 *
 * @todo
 * This file need complete refactoring.
 */

/**
 * Connect to the mail server.
 *
 * @param folder
 *   The name of the folder to open. If no folder is specified, the default
 *   of INBOX is used.
 *
 * @return
 *   The mailbox connection, if successful, or FALSE if not.
 */
function _basic_webmail_connect_to_server($folder = 'INBOX') {
  global $user;

  // Get the users e-mail login and password.
  $username = $user->basic_webmail_username;
  $password = $user->basic_webmail_password;

  // Sanity check. Make sure the user's information is available.
  if (!$username || !$password) {
    watchdog(
      'basic_webmail',
      'The user <a href="!url">@name</a> has tried to access Basic webmail
        interface, but his mail account data has not yet been configured.',
      array('!url' => url('user' . $user->uid), '@name' => $user->name),
      WATCHDOG_ERROR
    );
    return FALSE;
  }

  $mailbox = _basic_webmail_get_server_string() . $folder;

  // Make the actual connection.
  if (version_compare(phpversion(), '5.2.0', '>=')) {
    $imap_resource = imap_open($mailbox, $username, $password, 0, 1);
    if ($imap_resource == FALSE) {
      watchdog(
        'basic_webmail',
        'There was an error accessing the remote server: !last_error',
        array('!last_error' => imap_last_error()),
        WATCHDOG_ERROR
      );
      return FALSE;
    }
  }
  else {
    $imap_resource = imap_open($mailbox, $username, $password, 0);
    if ($imap_resource == FALSE) {
      watchdog(
        'basic_webmail',
        'There was an error accessing the remote server: !last_error',
        array('!last_error' => imap_last_error()),
        WATCHDOG_ERROR
      );
      return FALSE;
    }
  }

  return $imap_resource;
}

/**
 * Create the server connection string.
 *
 * @return
 *   The string representing the server connection information.
 */
function _basic_webmail_get_server_string() {
  $server_address = trim(variable_get('basic_webmail_server_address', ''));
  $server_port    = trim(variable_get('basic_webmail_server_port', ''));

  // Sanity check. Make sure the admin has done the configuration.
  if (($server_address == '') || ($server_port == '')) {
    watchdog(
      'basic_webmail',
      'The <a href="!url">server_settings</a> for Basic webmail have not yet
        been configured. Please do that before checking e-mails.',
      array('!url' => url('admin/settings/basic_webmail')),
      WATCHDOG_ERROR
    );
    return FALSE;
  }

  // Prepare the flags for connecting to the server with.
  $imap_flags = '';
  if (variable_get('basic_webmail_secure_log_in', 0) == 1) {
    $imap_flags .= '/secure';
  }
  if (variable_get('basic_webmail_use_ssl', 0) == 1) {
    $imap_flags .= '/ssl';
  }
  if (variable_get('basic_webmail_validate_cert', 0) == 1) {
    $imap_flags .= '/validate-cert';
  }
  else {
    $imap_flags .= '/novalidate-cert';
  }
  if (variable_get('basic_webmail_use_tls', 0) == 1) {
    $imap_flags .= '/tls';
  }
  else {
    $imap_flags .= '/notls';
  }

  // Return the configured settings and create a connection string.
  return ($server_port == '143')
    ? '{' . $server_address . $imap_flags . '}'
    : '{' . $server_address . ':' . $server_port . $imap_flags . '}';
}

/**
 * @note
 * Much of the following function was taken from
 * http://us2.php.net/manual/en/function.imap-fetchstructure.php#51497
 *
 * @todo
 * We need to get rid of next functions and use standard functions from php imap
 * library.
 */

/**
 * Get a list of all the possible parts.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 *
 * @return
 *   An array containing all of the available message parts, or FALSE if
 *   there is an error.
 */
function _basic_webmail_get_parts_list($imap_resource, $message_number) {

  // Initialize the return value.
  $parts_list = FALSE;

  // Retrieve the message structure.
  $message_structure = imap_fetchstructure($imap_resource, $message_number);
  if (!$message_structure) {
    $last_error = imap_last_error();
    watchdog('basic_webmail', 'There was an error retrieving the message structure: !last_error', array('!last_error' => $last_error), WATCHDOG_ERROR);
    return '<h2 class="title">' . t('Error') . '</h2><br />'. t('There was an error retrieving the message structure: !last_error', array('!last_error' => $last_error)) . '<br /><br />';
  }

  // Pull the parts from the message structure.
  $parts = $message_structure->parts;

  if (!$parts || $message_structure->type == 0) {
    // Simple message -- only 1 part.
    $parts_list[1] = array($message_structure->subtype);
  }
  else {
    // Complex message -- multiple parts.
    $endwhile = FALSE;
    // Stack while parsing message.
    $stack = array();
    // Incrementer.
    $i = 0;

    while (!$endwhile) {
      if (!$parts[$i]) {
        if (count($stack) > 0) {
          $stack_count = count($stack) - 1;
          $parts = $stack[$stack_count]['p'];
          $i     = $stack[$stack_count]['i'] + 1;
          array_pop($stack);
        }
        else {
          $endwhile = TRUE;
        }
      }

      if (!$endwhile) {
        // Create message part first (example '1.2.3').
        $partstring = '';

        foreach ($stack as $s) {
          $partstring .= ($s['i'] + 1) . '.';
        }

        $partstring .= ($i + 1);

        // Determine attachment status.
        // If type > 2 (0=plain & 1=html) then it's not a message attachment.
        if ($parts[$i]->type > 2) {
          // One or more attachments exists.
          $parts_list[$partstring] = array(drupal_strtoupper($parts[$i]->subtype), $parts[$i]);
        }
        else {
          // No attachments exist.
          $parts_list[$partstring] = array(drupal_strtoupper($parts[$i]->subtype));
        }
      }

      if ($parts[$i]->parts) {
        $stack[] = array('p' => $parts, 'i' => $i);
        $parts = $parts[$i]->parts;
        $i = 0;
      }
      else {
        $i++;
      }
    }
  }

  return $parts_list;
}

/**
 * Decode the body part for the specified message number and message part ID.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 * @param part_id
 *   The ID of the part to get the information for.
 *
 * @return
 *   The message body, decoded if necessary.
 */
function _basic_webmail_decode_body_part($imap_resource, $message_number, $part_id) {
  $message_body_structure = imap_bodystruct($imap_resource, $message_number, $part_id);
  switch ($message_body_structure->encoding) {
    case 3:
      return imap_base64(_basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id));
      break;

    case 4:
      return utf8_encode(quoted_printable_decode(_basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id)));
      break;

    default:
      return _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id);
      break;
  }
}


/**
 * Process the attachment for the specified message number and message part ID.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 * @param part_id
 *   The ID of the part to get the information for.
 * @param part_structure
 *   An object with the structure definitaion of the part of the message that
 *   represents the attachment.
 *
 * @return
 *   An array containing all of the available message parts, or FALSE if
 *   there is an error.
 */
function _basic_webmail_process_attachment($imap_resource, $message_number, $part_id, $part_structure) {
  $attachment = '';
  $file_name  = $part_structure->parameters[0]->value;
  $file_data  = _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id);

  if ($part_structure->encoding == 3) {
    // Decode if base64.
    $file_data = base64_decode($file_data);
  }
  elseif ($part_structure->encoding == 4) {
    $file_data = quoted_printable_decode($file_data);
  }

  $file_dest = basic_webmail_get_att_path($imap_resource, $message_number);
  $file_path  = file_save_data($file_data, $file_dest . '/' . $file_name, FILE_EXISTS_REPLACE);
  $file_url   = file_create_url($file_path);
  $attachment_id = trim($part_structure->id, '<>');
  $attachment = array($file_path, $file_name, $file_url, $attachment_id);

  return $attachment;
}

// Get the body of a part of a message according to the string in $part.
function _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part) {
  $parts        = _basic_webmail_mail_fetchparts($imap_resource, $message_number);
  $part_numbers = explode(".", $part);
  $current_part = $parts;

  while (list($key, $val) = each($part_numbers)) {
    $current_part = $current_part[$val];
  }

  if ($current_part != "") {
    return $current_part;
  }
  else {
    return FALSE;
  }
}

// Get an array with the bodies of all parts of an email.
// The structure of the array corresponds to the structure that is available
// with imap_fetchstructure.
function _basic_webmail_mail_fetchparts($imap_resource, $message_number) {
  $parts  = array();
  $header = imap_fetchheader($imap_resource, $message_number);
  $body   = imap_body($imap_resource, $message_number, FT_INTERNAL);
  $i      = 1;

  $new_parts = _basic_webmail_mail_mimesplit($header, $body);
  if ($new_parts) {
    while (list ($key, $val) = each($new_parts)) {
      $parts[$i] = _basic_webmail_mail_mimesub($val);
      $i++;
    }
  }
  else {
    $parts[$i] = $body;
  }

  return $parts;
}


// Splits a message given in the body if it is a mulitpart mime message and
// returns the parts, if no parts are found, returns false.
function _basic_webmail_mail_mimesplit($header, $body) {
  $parts            = array();
  $PN_EREG_BOUNDARY = "Content-Type:(.*)boundary=\"([^\"]+)\"";
  $result = eregi($PN_EREG_BOUNDARY, $header, $regs);

  if(!$result) {
    $PN_EREG_BOUNDARY = "Content-Type:(.*)";
    $result = eregi($PN_EREG_BOUNDARY, $header, $regs);
  }
  if ($result) {
    $boundary      = isset($regs[2]) ? $regs[2] : '';
    $delimiter_reg = "([^\r\n]*)$boundary([^\r\n]*)";

    if (eregi($delimiter_reg, $body, $results)) {
      $delimiter = $results[0];
      $parts     = explode($delimiter, $body);
      $parts     = array_slice($parts, 1, -1);
    }

    return $parts;
  }
  else {
    return FALSE;
  }
}


// Returns an array with all parts that are subparts of the given part.
// If no subparts are found, returns the body of the current part.
function _basic_webmail_mail_mimesub($part) {
  $i              = 1;
  $head_delimiter = "\r\n\r\n";
  $del_length     = drupal_strlen($head_delimiter);

  // get head & body of the current part
  $end_of_head = strpos($part, $head_delimiter);
  $head        = drupal_substr($part, 0, $end_of_head);
  $body        = drupal_substr($part, $end_of_head + $del_length, drupal_strlen($part));

  // check whether it is a message according to rfc822
  if (stristr($head, "Content-Type: message/rfc822")) {
    $part            = drupal_substr($part, $end_of_head + $del_length, drupal_strlen($part));
    $return_parts[1] = _basic_webmail_mail_mimesub($part);
    return $return_parts;
  }
  // if no message, get subparts and call function recursively
  elseif ($sub_parts = _basic_webmail_mail_mimesplit($head, $body)) {
    // got more subparts
    while (list ($key, $val) = each($sub_parts)) {
      $return_parts[$i] = _basic_webmail_mail_mimesub($val);
      $i++;
    }

    return $return_parts;
  }
  else {
    return $body;
  }
}
/*
  End of four imap_fetchbody functions.
 */


/**
 * Checks for the existence of a specified mailbox.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param mailbox
 *   The name of the mailbox to check for.
 *
 * @return
 *   Returns TRUE if the mailbox exists, and FALSE if it does not exist.
 */
function _basic_webmail_mailbox_exists($imap_resource, $mailbox) {
  $mailbox_exists = FALSE;

  $mailboxes = imap_getmailboxes($imap_resource, _basic_webmail_get_server_string(), '*');
  foreach ($mailboxes as $mailbox_object) {
    if (drupal_substr($mailbox_object->name, strpos ($mailbox_object->name, '}') + 1, drupal_strlen($mailbox_object->name)) == $mailbox) {
      $mailbox_exists = TRUE;
    }
  }

  return $mailbox_exists;
}


/**
 * Checks if the standard mailboxes exit and creates them, if necessary.
 *
 * This function checks to see if the standard mailboxes exit or not. If they
 * do not exist, this function will also create them. This fucntion will then
 * subscribe to the mailboxes.
 */
function _basic_webmail_subscribe_to_mailboxes() {
  // Connect to the server and retrieve a connection to the mailbox.
  $imap_resource = _basic_webmail_connect_to_server();
  if (!$imap_resource) {
    drupal_set_title(t('Error'));
    return '<br />' . t('There was an error connecting to the mail server. Contact the system administrator and/or check the logs for more information.') . '<br /><br />';
  }

  $mailboxes = array('INBOX', 'INBOX.Drafts', 'INBOX.Junk', 'INBOX.Sent', 'INBOX.Trash');
  foreach ($mailboxes as $mailbox) {
    if (!imap_status($imap_resource, _basic_webmail_get_server_string() . $mailbox, SA_ALL)) {
      if (!imap_createmailbox($imap_resource, _basic_webmail_get_server_string() . $mailbox)) {
        watchdog('basic_webmail', 'There was an error subscribing to the INBOX mailbox: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
      }
    }

    if (!imap_subscribe($imap_resource, _basic_webmail_get_server_string() . $mailbox)) {
      watchdog('basic_webmail', 'There was an error subscribing to the !mailbox mailbox: !last_error', array('!mailbox' => $mailbox, '!last_error' => imap_last_error()), WATCHDOG_WARNING);
    }
  }

  // Clean up.
  if (!imap_close($imap_resource)) {
    watchdog('basic_webmail', 'There was an error closing the IMAP stream: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
  }
}


/**
 * Converts UTF8 encoded strings.
 *
 * This function is necessary, because there is a bug in some versions of PHP
 * that returns strings converted with the imap_utf8() function in all upper
 * case letters.  http://bugs.php.net/bug.php?id=44098
 *
 * This function was taken from here and modified:
 * http://us2.php.net/manual/en/function.imap-utf8.php#92026
 */
function _basic_webmail_decode_mime_str($string, $char_set='UTF-8') {
  $new_string = '';
  $elements  = imap_mime_header_decode($string);

  for($i = 0; $i < count($elements); $i++) {
    if ($elements[$i]->charset == 'default') {
          $elements[$i]->charset = 'iso-8859-1';
    }

    $new_string .= iconv($elements[$i]->charset, $char_set, $elements[$i]->text);
  }

  return $new_string;
}

/**
 * Helper. Returns formatted address.
 *
 * @param $addresses
 *   An object gotten with imap_headerinfo(). Should contain the following
 *   properties: personal, adl, mailbox, and host.
 *   OR. An array with such objects.
 * @param $as_link
 *   Boolean. Defines do we need format address as a HTML link.
 *
 * @return
 *   A string with formatted addresses separated by commas (safe for output).
 */
function basic_webmail_format_address($addresses, $as_link) {
  $addresses = is_array($addresses) ? $addresses : array($addresses);
  $array = array();

  foreach ($addresses as $address) {
    $personal = isset($address->personal)
      ? _basic_webmail_decode_mime_str($address->personal) : '';
    $email = $address->mailbox . '@' . $address->host;
    $summary = $personal ? $personal . ' <' . $email . '>' : $email;

    $array[] = $as_link
      ? l($personal ? $personal : $email, 'basic_webmail/sendmail/' . $summary)
      : check_plain($summary);
  }

  return empty($array) ? '' : implode(', ', $array);
}

/**
 * Helper. Returns attachments path for the given message.
 */
function basic_webmail_get_att_path($imap_resource, $message_number, $uid = NULL) {
  global $user;

  if ($uid === NULL) {
    $uid = $user->uid;
  }

  // Create all nedded directories.
  $path = variable_get('basic_webmail_attachment_location', file_directory_path() . '/attachments');
  file_check_directory($path, FILE_CREATE_DIRECTORY);
  $path = $path . '/' . $uid;
  file_check_directory($path, FILE_CREATE_DIRECTORY);
  $message_overview = imap_fetch_overview($imap_resource, $message_number);
  $path = $path . '/' . trim($message_overview[0]->message_id, '<>');
  file_check_directory($path, FILE_CREATE_DIRECTORY);

  // file_check_directory() has an annoying habit of displaying "directory ... 
  // has been created" status messages. To avoid confusing visitors we clear 
  // out all the status messages. This might affect some other messages but
  // errors and warnings should still be displayed.
  drupal_get_messages('status', TRUE);

  return $path;
}

/**
 * Recursively delete all files and folders in the specified filepath, then
 * delete the containing folder. Borrowed from Webform module.
 *
 * @note
 *   This only deletes visible files with write permission.
 *
 * @param string $path
 *   A filepath relative to file_directory_path
 */
function basic_webmail_recursive_delete($path) {
  if ($path && is_dir($path)) {
    $listing = $path . '/*';
    foreach (glob($listing) as $file) {
      if (is_file($file) === TRUE) {
        @unlink($file);
      }
      elseif (is_dir($file) === TRUE) {
        basic_webmail_recursive_delete($file);
      }
    }
    @rmdir($path);
  }
}
